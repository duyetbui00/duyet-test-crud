@extends('layouts.app')

@section('content')
<h2 >Role Management</h2>


    <div class="container">
        <div class=" w-100 d-flex ">
            <form action="{{route('roles.index')}}" method="get" class="d-flex align-items-center flex-nowrap" >
                <input type="text"name="key" placeholder="Search by name..."  class="form-control">
                <button class="btn btn-dark" type="submit"><i class="fa-solid fa-magnifying-glass"></i></button>
            </form>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th width="280px"> @can('role-create')
                <a class="btn btn-success" href="{{ route('roles.create') }}"> Create New Role </a>
            @endcan</th>
        </tr>
            @foreach ($roles as $key => $role)
            <tr>
                <td>{{ $role->id }}</td>
                <td>{{ $role->name }}</td>
                <td>
                    <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Show</a>
                    {{-- @can('role-edit')
                        <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a>
                    @endcan
                    @can('role-delete')
                        {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    @endcan --}}
                </td>
            </tr>
        @endforeach
    </table>

    {{-- {!! $roles->render() !!} --}}
    {{ $roles->appends(request()->all())->links()}}


@endsection
