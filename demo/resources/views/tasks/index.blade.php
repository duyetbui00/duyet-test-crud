@extends('layouts.app')


@section('content')

    <div class="container">
        <div class=" w-100 d-flex ">
            <form  class="d-flex align-items-center flex-nowrap" >
                <input type="text" id="search" placeholder="Search by name..."  class="form-control">
                <button class="btn btn-dark" id="btn-search" type="submit"><i class="fa-solid fa-magnifying-glass"></i></button>
            </form>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <table class="table table-striped ">
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Content</th>
                 <th><a class="btn btn-success"   data-bs-toggle="modal" data-bs-target="#btn-create" onclick="viewFromCreate()"><i class="fa-solid fa-plus"></i></a></th>
                </tr>
                @foreach ($tasks as $task)
                    <tr>
                        <th>{{$task->id}}</th>
                        <th>{{$task->name}}</th>
                        <th>{{$task->content}}</th>
                        <th>
                            <form method="POST" action="tasks/destroy/{{$task->id}}">
                                @csrf
                                @method("DELETE")
                                <a  data-bs-toggle="modal" data-bs-target="#btn-remove"  onclick="viewDeleteTask({{ $task->id }})" class="btn btn-danger"><i class="fa-solid fa-minus"></i></i></a>
                                <a  data-bs-toggle="modal" data-bs-target="#btn-create"  onclick="viewUpdateTask({{ $task->id }})" class="btn btn-warning"><i class="fa-solid fa-pen"></i></a>
                            </form>
                        </th>
                    </tr>
                @endforeach
            </table>
         {{$tasks->appends(request()->all())->links()}}
        </div>
    </div>

    {{-- modal-create-update --}}
    <div class="modal fade" id="btn-create" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">`
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create New Tasks</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                    aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data">
                    <div class="message"></div>
                    <div class="mb-3">
                        <label class="form-label">Name</label>
                        <input id="task-name" type="text" class="form-control">
                        <div id="task-name-message" class="valid-feedback"></div>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Content</label>
                        <input id="task-content" type="text" class="form-control">
                        <div id="task-content-message" class="valid-feedback"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="btn-close" data-bs-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-primary" id="btn-add" onclick="createTask()">
                    Add
                </button>
                <button type="button" class="btn btn-primary d-none" id="btn-update">Save</button>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>

            <div class="modal fade" id="btn-remove" tabindex="-1" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Task delete</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            Do you want to delete it?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">No</button>
                            <button type="button" id="submit-remove" onclick="deleteTask()"
                                    class="btn btn-primary">Yes</button>
                        </div>
                    </div>
                </div>
            </div>

    <script type="text/javascript" charset="utf-8">
         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //Search
        $(document).ready(function() {
            $("#btn-search").click(function(e) {
                e.preventDefault();
                var $value = $("#search").val();
                $.ajax({
                    type: 'get',
                    data: {
                        'search': $value,
                    },
                    url: '{{ route('tasks.index') }}',
                    statusCode: '200',
                    success: function(data) {
                        if ($("body").html(data)) {
                            $("table").fadeOut(10, function() {
                                $("table").fadeIn();
                            });
                        }
                    },
                });
            });
        });

        //Create
        function viewFromCreate() {
            $.ajax({
                method: 'get',
                url: '{{ route('tasks.create') }}',
                success: function(data) {
                    $("#exampleModalLabel").html("Create task");
                    $("#task-name").val('').removeClass('is-invalid');
                    $("#task-content").val('').removeClass('is-invalid');
                    $(".alert").remove();
                    $("#btn-update").addClass("d-none").removeAttr("onclick");
                    $("#btn-add").removeClass("d-none");
                    $("#btn-add").attr("onclick", "createTask()");
                    $("#btn-close").click(function() {
                        $("body").load("/tasks");
                    });
                    $("#btn-create.modal.fade.show").click(function() {
                        $("body").load("/tasks");
                    });
                }
            });
        }

        function createTask() {
            // $.ajaxSetup({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     }
            // });
            var formData = new FormData();
            formData.append('name', $("#task-name").val());
            formData.append('content', $("#task-content").val());
            $.ajax({
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                type: 'post',
                data: formData,
                url: '{{ route('tasks.store') }}',
                success: function(data) {
                    $(".message").html(
                        '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                        data.message +
                        '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>' +
                        '</div>');
                        setTimeout(function() {
                        window.location.reload();
                    }, 1000);
                },
                error: function(data) {
                    validateForm(data);
                }
            });
        }

        // Update
        function viewUpdateTask(id) {
            $.ajax({
                type: 'get',
                dataType: 'json',
                url: '/tasks/edit/' + id,
                success: function(data) {
                    $("#exampleModalLabel").html("Edit task");
                    $("#task-name").val(data.tasks.name).removeClass("is-invalid");
                    $("#task-content").val(data.tasks.content).removeClass("is-invalid");
                    $(".alert").remove();
                    $("#btn-add").addClass("d-none").removeAttr("onclick");
                    $("#btn-update").removeClass("d-none").attr("onclick", "updateTasks(" + id + ")");
                    $("#btn-close").click(function() {
                        $("body").load("/tasks");
                    });
                    $("#btn-create.modal.fade.show").click(function() {
                        $("body").load("/tasks");
                    });
                }
            });
        }

        function updateTasks(id) {
            // $.ajaxSetup({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            //     }
            // });
            var formData = new FormData();
            formData.append('_method', 'PUT');
            formData.append('name', $("#task-name").val());
            formData.append('content', $("#task-content").val());
            $.ajax({
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                type: 'post',
                data: formData,
                url: '/tasks/update/' + id,
                success: function(data) {
                    $(".message").html(
                        '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                        data.message +
                        '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>' +
                        '</div>');
                    setTimeout(function() {
                        window.location.reload();
                    }, 1000);
                },
                error: function(data) {
                    validateForm(data);
                }
            });
        }
        // Delete
        function viewDeleteTask(id) {
            $("#submit-remove").val(id);
        }

        function deleteTask() {
            // $.ajaxSetup({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            //     }
            // });
            var id = $("#submit-remove").val();
            $.ajax({
                type: "delete",
                url: "/tasks/destroy/" + id,
                success: function(data) {
                    window.location.reload();
                }
            })
        }

        function validateForm(data) {
            $(".message").html('<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                data.responseJSON.message +
                '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>' +
                '</div>')
            if (data.responseJSON.errors.name) {
                $("#task-name").addClass("is-invalid");
                $("#task-name-message").html(data.responseJSON.errors.name)
                    .removeClass("valid-feedback").addClass("invalid-feedback");
            }
            if (data.responseJSON.errors.content) {
                $("#task-content").addClass("is-invalid");
                $("#task-content-message").html(data.responseJSON.errors.content)
                    .removeClass("valid-feedback").addClass("invalid-feedback");
            }
        }


    </script>
@stop
