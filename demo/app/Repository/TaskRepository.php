<?php

namespace App\Repository;

use App\Models\Task;

class TaskRepository implements IRepository
{
    private $task;
    public function __construct(Task $task )
    {
        $this->task= $task;

    }

    public function paginate()
    {
      return $this->task->latest('id')->Search()->paginate(10);
    }

    public function all()
    {
        return $this->task->all();
    }

    public function create($data)
    {
       return $this->task->create($data);
    }

    public function update($data, $id)
    {
        return $this->task->findOrFail($id)->update($data);
    }

    public function delete($id)
    {
       return $this->task->findOrFail($id)->delete();
    }

    public function show($id)
    {
        return $this->task->findOrFail($id);
    }
}
