<?php

namespace App\Repository;



use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleRepository implements IRepository
{
    private $role ,$permission;
    public function __construct(Role $role,Permission $permission)
    {
        $this->role=$role;
        $this->permission=$permission;
    }

    public function paginate()
    {
      return $this->role->orderBy('id','DESC')->Search()->paginate(5);

    }

    public function all()
    {
        // TODO: Implement all() method.
    }

    public function create($data)
    {
        // TODO: Implement create() method.
    }

    public function update($data, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function show($id)
    {
       return $this->role->find($id)->where->where('id')->getPermission($id);
        // $this->permission->join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
        // ->where("role_has_permissions.role_id",$id);

    }


}
