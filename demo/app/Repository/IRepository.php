<?php

namespace App\Repository;

interface IRepository
{
    public function paginate();

    public function all();

    public function create($data);

    public function update($data, $id);

    public function delete($id);

    public function show($id);

}
