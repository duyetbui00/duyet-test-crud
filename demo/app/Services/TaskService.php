<?php

namespace App\Services;

use App\Repository\TaskRepository;

class TaskService implements IService
{
    private $repo;
    public function __construct(TaskRepository $repo)
    {
        $this->repo=$repo;
    }

    public function paginate()
    {
       return $this->repo->paginate();
    }

    public function create($data)
    {
        return $this->repo->create($data);
    }

    public function update($data, $id)
    {
        return $this->repo->update($data,$id);
    }

    public function delete($id)
    {
        return $this->repo->delete($id);
    }


    public function show($id)
    {
        return $this->repo->show($id);
    }


    public function all()
    {
        return $this->repo->all();
    }

}
