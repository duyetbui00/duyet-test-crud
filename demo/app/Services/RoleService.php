<?php

namespace App\Services;

use App\Repository\RoleRepository;

class RoleService implements IService
{
    private $repo ;
    public function __construct(RoleRepository $repo)
    {
        $this->repo=$repo;
    }

    public function paginate()
    {
        return $this->repo->paginate();
    }

    public function create($data)
    {
        return $this->repo->create($data);
    }

    public function update($data, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function show($id)
    {
        // TODO: Implement show() method.
    }

    public function all()
    {
        // TODO: Implement all() method.
    }
}
