<?php

namespace App\Http\Controllers;

use App\Services\RoleService;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;


class RoleController extends Controller
{
    protected $service;

    function __construct(RoleService $service)
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
        $this->service=$service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = $this->service->paginate();
        return view('roles.index',compact('roles'));
    }


    public function create()
    {
        $permission = $this->permission->all();
        return view('roles.create',compact('permission'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);

        $role->name = $request->input('name');
        $role->save();
        $role->syncPermissions($request->input('permission'));                   </div>
        </div>
    </div>
</div>
        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));

        return redirect()->route('roles.index')
            ->with('success','Role created successfully');
    }


    public function show($id)
    {
        $role=$this->service->show($id);
        return view('roles.show',compact('role'));
    }


    // public function edit($id)
    // {
    //     $role = Role::find($id);
    //     $permission = Permission::get();
    //     $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
    //         ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
    //         ->all();

    //     return view('roles.edit',compact('role','permission','rolePermissions'));
    // }


    // public function update(Request $request, $id)
    // {
    //     $this->validate($request, [
    //         'name' => 'required',
    //         'permission' => 'required',
    //     ]);

    //     $role = Role::find($id);
    //     $role->name = $request->input('name');
    //     $role->save();

    //     $role->syncPermissions($request->input('permission'));

    //     return redirect()->route('roles.index')
    //         ->with('success','Role updated successfully');
    // }

    // public function destroy($id)
    // {
    //     DB::table("roles")->where('id',$id)->delete();
    //     return redirect()->route('roles.index')
    //         ->with('success','Role deleted successfully');
    // }
}
