<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\EditTaskRequest;
use App\Models\Task;
use App\Services\TaskService;
use Illuminate\Http\Request;

class TaskController extends Controller
{
   protected $service;

    public function __construct(TaskService $service)
    {
        $this->service=$service;
   }

    public function index()
    {
        $tasks=$this->service->paginate();
        return view('tasks.index',compact('tasks'));
        // return response()->json(['tasks'=>$tasks],200);

   }

    public function store(EditTaskRequest $request)
    {
        $this->service->create($request->all());
        // return redirect()->route('tasks.index');
        return response()->json(['message' => 'Create task success!!'], 200);
   }

    public function create()
    { }

    public function update(EditTaskRequest $request,$id)
    {
        $this->service->update($request->all(),$id);
        return response()->json(['message' => 'Update task success!!'], 200);
   }

    public function edit($id)
    {
        $tasks = $this->service->show($id);
        return response()->json(['tasks' => $tasks], 200);
   }

    public function destroy($id)
    {
        $this->service->delete($id);
        return response()->json(null, 204);

   }


}
