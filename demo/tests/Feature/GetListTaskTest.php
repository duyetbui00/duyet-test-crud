<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetListTaskTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_can_get_all_task()
    {
        $task=Task::factory()->create();
        $response = $this->get($this->getListTaskRoute());
        $response->assertStatus(200);
        $response->assertViewIs('tasks.index');
        $response->assertSee($task->name);
    }

    public function getListTaskRoute()
    {
            return route('tasks.index');
    }
}
