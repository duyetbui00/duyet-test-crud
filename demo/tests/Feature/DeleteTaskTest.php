<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_auth_user_can_delete_task()
    {   $this->actingAs(User::factory()->create());
        $task=Task::factory()->create();
        $response=$this->delete($this->getDeleteTasKRoute($task->id));
        $this->assertDatabaseMissing('tasks',['id'=>$task->id]);
        $response->assertStatus(204);
    }

    public function test_unauth_user_can_not_delete_task()
    {
        $task=Task::factory()->create();
        $response=$this->delete($this->getDeleteTasKRoute($task->id));
        $response->assertRedirect('/login');
    }

    public function getDeleteTasKRoute($id)
    {
        return route('tasks.destroy',$id) ;
    }
}
