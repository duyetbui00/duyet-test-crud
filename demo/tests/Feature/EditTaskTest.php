<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EditTaskTest extends TestCase
{
    public function getEditTaskRoute($id)
    {
        return route('tasks.update',$id);
    }

    public function getEditTaskViewRoute($id)
    {
        return route('tasks.edit',$id);
    }
    public function test_auth_user_can_edit_task()
    {
        $this->actingAs(User::factory()->create());
        $task=Task::factory()->create();
        $response = $this->put($this->getEditTaskRoute($task->id),$task->toArray());
        $response->assertStatus(200);

    }

    public function test_unauth_user_can_not_edit_task()
    {
        $task=Task::factory()->create();
        $response = $this->put($this->getEditTaskRoute($task->id),$task->toArray());
        $response->assertRedirect('/login');
    }

    public function test_auth_user_can_not_edit_task_if_name_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task=Task::factory()->create();
        $data=['name'=>null,'content'=>$task->content];
        $response=$this->put($this->getEditTaskRoute(($task->id),$data));
        $response->assertSessionHasErrors(['name']);

    }

    public function test_auth_user_can_see_required_text_if_validate_error_editform()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create(['name'=>null,'content'=>null])->toArray();
        $response= $this->from($this->getEditTaskViewRoute($task['id']))
            ->put($this->getEditTaskRoute($task['id']),$task);
        $response->assertRedirect($this->getEditTaskViewRoute($task['id']));


    }
}
