<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateNewTaskTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_auth_user_can_new_task()
    {
        $this->actingAs(User::factory()->create());
        $task=Task::factory()->make()->toArray();
        $response = $this->post($this->getCreateTaskRoute(),$task);
        $response->assertStatus(200);

    }

    public function test_unauth_user_can_not_create_task()
    {
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->getCreateTaskRoute(), $task);
        $response->assertRedirect('/login');

    }

    public function test_auth_user_can_not_create_task_if_name_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task=Task::factory()->make(['name'=>null])->toArray();
        $response=$this->post($this->getCreateTaskRoute(),$task);
        $response->assertSessionHasErrors(['name']);
    }

    public function test_auth_user_can_see_required_text_if_validate_error_createform()
    {
        $this->actingAs(User::factory()->create());
        $task=Task::factory()->make(['name'=>null])->toArray();
        $response= $this->from($this->getCreateTaskViewRoute())->post($this->getCreateTaskRoute(),$task);
        $response->assertRedirect($this->getCreateTaskViewRoute());

    }

    public function getCreateTaskRoute()
    {
        return route('tasks.store');
    }

    public function getCreateTaskViewRoute()
    {
        return route('tasks.create');
    }
}
